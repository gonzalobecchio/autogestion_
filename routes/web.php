<?php
/**
 * This file contains all the routes for the project
 */

use Demo\Router;

Router::csrfVerifier(new \Demo\Middlewares\CsrfVerifier());

Router::setDefaultNamespace('\Demo\Controllers');

Router::group(['exceptionHandler' => \Demo\Handlers\CustomExceptionHandler::class], function () {
	Router::group(['prefix' => '/autogestion_'], function () {
        Router::get('/', 'DefaultController@home')->setName('home');

        Router::get('/contact', 'DefaultController@contact')->setName('contact');

        Router::basic('/companies/{id?}', 'DefaultController@companies')->setName('companies');	
    });

});

