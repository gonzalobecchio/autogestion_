<?php
namespace Demo\Controllers;

use Demo\Controllers\AllControllers;

class DefaultController extends AllControllers 
{

	public function index()
	{
		return $this->twig->render('index/index.html.twig');
		// return 'y?';
	}

	public function home(): string
	{
		// implement
		return sprintf('DefaultController -> index (?fun=%s)', input('fun'));
	}

	public function contact(): string
	{
		return 'DefaultController -> contact';
	}

	public function companies($id = null): string
	{
		return 'DefaultController -> companies -> id: ' . $id;
	}

	public function notFound(): string
	{
		return 'Page not found';
	}


}