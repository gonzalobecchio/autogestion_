<?php  
namespace Demo\Controllers;

class AllControllers 
{
    public $twig;

    function __construct()
    {
        $loader = new \Twig\Loader\FilesystemLoader(realpath('../app/Templates'));
        $this->twig = new \Twig\Environment($loader);
    }
    
}
